#!/bin/bash

set -e

rake db:migrate
rake rubocop
rake spec
